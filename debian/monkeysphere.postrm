#!/bin/sh

# postrm script for monkeysphere

# Author: Jameson Rollins <jrollins@finestructure.net>, 
#         Daniel Kahn Gillmor <dkg@fifthhorseman.net>
# Copyright 2008-2019

set -e

case $1 in
    purge)
        # protect users from locking themselves out:
        if which sshd >/dev/null && sshd -T | grep -i '^authorizedkeysfile.*/var/lib/monkeysphere' >&2 ; then
            echo >&2 ""
            echo >&2 "  Monkeysphere Warning!"
            echo >&2 "  ====================="
            echo >&2 "  Not purging monkeysphere because sshd is configured to rely on it."
            echo >&2 "  Please remove AuthorizedKeysFile references to /var/lib/monkeysphere"
            echo >&2 "  from sshd_config before purging the monkeysphere package."
            echo >&2 ""
            exit 1
        fi
	# delete monkeysphere user
	# https://wiki.debian.org/AccountHandlingInMaintainerScripts
	if which deluser >/dev/null 2>&1 ; then
	    deluser --quiet --system monkeysphere > /dev/null || true
	else
	    echo >&2 "not removing monkeysphere system account because deluser command was not found"
	fi
        rm -rf /var/lib/monkeysphere
        ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
